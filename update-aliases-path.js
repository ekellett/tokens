const fs = require('fs');

const pathArg = process.argv.slice(2);
const importObj = {"imports": pathArg};
const jsonContent = JSON.stringify(importObj);

fs.writeFile("./design-tokens/aliases.json", jsonContent, 'utf8', function (err) {
    if (err) {
        console.log("An error occured while writing JSON Object to File.");
        return console.log(err);
    }

    console.log("JSON file has been saved.");
});
