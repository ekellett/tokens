# design-tokens
 ##Installation

* Run npm install
* Tokens are generated using npm run build-design or npm run build-code. This will create 2 folders, dist containing the design tokens and docs contains the styleguide. 
 [Design Theme](https://kellettemma.github.io/design-tokens/design-theme/design-tokens.html),
 [Code Theme](https://kellettemma.github.io/design-tokens/code-theme/design-tokens.html)
